package edu.uga.cs.csci6900.finalProject.io;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.Writable;

/* This class is very similar to WordQuant except that it uses a double */
public class WordMeasure implements Writable {
	String word;
	double measure;
	
	/* Constructors */
	public WordMeasure() {
		
	}
	public WordMeasure(String pWord) {
		word = pWord;
		measure = 0.0;
	}
	
	public WordMeasure(String pWord, double pMeasure) {
		word = pWord;
		measure = pMeasure;
	}
	
	
	
	public String getWord() {
		return word;
	}
	public void setWord(String word) {
		this.word = word;
	}
	public double getMeasure() {
		return measure;
	}
	public void setMeasure(double measure) {
		this.measure = measure;
	}
	@Override
	public void readFields(DataInput in) throws IOException {
		
		word = in.readUTF();
		measure = in.readDouble();
	}

	@Override
	public void write(DataOutput out) throws IOException {
		out.writeUTF(word);
		out.writeDouble(measure);

	}

}
