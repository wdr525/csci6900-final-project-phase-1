package edu.uga.cs.csci6900.finalProject.io;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.Writable;

public class PositionValue implements Writable {

	private int position;
	private double value;
	
	public PositionValue() {
		
	}
	
	public PositionValue(int pPosition, double pValue) {
		position = pPosition;
		value = pValue;
	}
	
	@Override
	public void readFields(DataInput in) throws IOException {
		position = in.readInt();
		value = in.readDouble();
		
	}

	@Override
	public void write(DataOutput out) throws IOException {
		out.writeInt(position);
		out.writeDouble(value);

	}

}
