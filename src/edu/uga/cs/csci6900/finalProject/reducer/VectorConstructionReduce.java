package edu.uga.cs.csci6900.finalProject.reducer;

import java.io.IOException;
import java.util.HashMap;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.Reducer.Context;

import edu.uga.cs.csci6900.finalProject.io.WordMeasure;
import edu.uga.cs.csci6900.finalProject.io.WordQuant;

public class VectorConstructionReduce extends
		Reducer<Text, WordMeasure, Text, Text> {

	@Override
	protected void reduce(Text keyIn, Iterable<WordMeasure> valsIn,
			Context context)
			throws IOException, InterruptedException {
		
		
		HashMap<String, Double> wordMap = new HashMap<String, Double>();
		
		/* Iterate over the entries, merging duplicates */
		for (WordMeasure wq : valsIn) {
			if (!wordMap.containsKey(wq.getWord())) {
				wordMap.put(wq.getWord(), new Double(wq.getMeasure()));
			} else {
				
				System.err.println("Warning: Duplicate words during vector construction");
			}
			
		}
		
		/* Construct a string of words to be written to file */
		StringBuilder quantsStr = new StringBuilder();
		for (String word : wordMap.keySet()) {
			quantsStr.append(measureStr(word, wordMap.get(word)));
			quantsStr.append(' ');
		}
		context.write(keyIn, new Text(quantsStr.toString()));
		//context.write(new Text(keyIn.toString()+"("+Integer.toString((int)totalWords)+")"), new Text(quantsStr.toString()));
	}

	private String measureStr(String word, Double value) {
		
		return "\""+word+"\"("+value.toString()+")";
	}
}
