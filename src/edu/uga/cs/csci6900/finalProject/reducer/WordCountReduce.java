package edu.uga.cs.csci6900.finalProject.reducer;

import java.io.IOException;
import java.util.HashMap;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import edu.uga.cs.csci6900.finalProject.TextProcessor.TextProcessCounters;
import edu.uga.cs.csci6900.finalProject.io.WordQuant;

public class WordCountReduce extends Reducer<Text, WordQuant, Text, Text> {

	@Override
	protected void reduce(Text keyIn, Iterable<WordQuant> valsIn,
			Context context)
			throws IOException, InterruptedException {

		HashMap<String, Integer> wordMap = new HashMap<String, Integer>();
		
		/* Iterate over the entries, merging duplicates */
		for (WordQuant wq : valsIn) {
			if (!wordMap.containsKey(wq.getWord())) {
				wordMap.put(wq.getWord(), new Integer(wq.getCount()));
			} else {
				Integer crrntCount = wordMap.get(wq.getWord());
				wordMap.put(wq.getWord(), new Integer(crrntCount.intValue()+wq.getCount()));
			}

		}
		
		/* Construct a string of words to be written to file */
		StringBuilder quantsStr = new StringBuilder();
		for (String word : wordMap.keySet()) {
			quantsStr.append(quantStr(word, wordMap.get(word)));
			quantsStr.append(' ');
		}
		
		context.getCounter(TextProcessCounters.QUANT_STOCK_SYMBOLS).increment(1);
		context.write(keyIn, new Text(quantsStr.toString()));
	}

	private String quantStr(String word, Integer count) {
		return "\""+word+"\"("+Integer.toString(count)+")";
	}
	
}
