package edu.uga.cs.csci6900.finalProject;


import java.net.URI;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import edu.uga.cs.csci6900.finalProject.io.PositionValue;
import edu.uga.cs.csci6900.finalProject.io.WordMeasure;
import edu.uga.cs.csci6900.finalProject.io.WordQuant;
import edu.uga.cs.csci6900.finalProject.mapper.FilterWordsMap;
import edu.uga.cs.csci6900.finalProject.mapper.MatrixConstructionMap;
import edu.uga.cs.csci6900.finalProject.mapper.WordCountMap;
import edu.uga.cs.csci6900.finalProject.reducer.*;

/* This class is the driver that processes raw text into a set of vectors */
public class TextProcessor extends Configured implements Tool {
	private static final String DEFAULT_STOP_LIST_LOC = "stop_words.txt";
	
	private static final String DEFAULT_COUNT_SRC = "articles";
	private static final String DEFAULT_COUNT_DEST = "word_counts";
	private static final String DEFAULT_FILTER_WORDS_DEST = "filtered_words";
	private static final String DEFAULT_VECTORS_DEST = "vectors";
	
	private static final String WHITE_LIST_KEY = "filtered_words";
	private static final String STOP_LIST_KEY = "stop_list";
	private static final String QUANT_SYMBOLS_KEY = "quant_symbols";
	
	private String stopWordSrc = DEFAULT_STOP_LIST_LOC;
	
	private String countSrc = DEFAULT_COUNT_SRC;
	private String countDest = DEFAULT_COUNT_DEST;
	
	private String filterSrc = DEFAULT_COUNT_DEST;
	private String filterDest = DEFAULT_FILTER_WORDS_DEST;
	
	private String vectorSrc = DEFAULT_COUNT_DEST;
	private String vectorDest = DEFAULT_VECTORS_DEST;
	
	private long quantSymbols = 0;
	
	public enum TextProcessCounters {
		QUANT_STOCK_SYMBOLS
	}
	
	@Override
	public int run(String[] args) throws Exception {
		parseArguments(args);
		/* Build the set of word counts for each symbol */
		buildWordCounts(countSrc, stopWordSrc, countDest);
		
		/* Filter the unimportant words */
		filterWords(filterSrc, filterDest);
		
		constructVectors(vectorSrc, filterDest, vectorDest);
				
		return 0;
	}

	private void parseArguments(String[] args) {
		int start;
		for (String arg : args) {
			if (arg.matches("articles=.+")) {
				start = arg.indexOf('=')+1;
	
				countSrc = arg.substring(start);
			} else if (arg.matches("stopwords=.+")) {
				start = arg.indexOf('=')+1;
				stopWordSrc = arg.substring(start);
			} else if (arg.matches("vectors=.+")) {
				start = arg.indexOf('=')+1;
				vectorDest = arg.substring(start);
			}
		}
	}
	
	private int buildWordCounts(String src, String stopWordsSrc, String dest) throws Exception {
		
		boolean success;
		Job countJob = Job.getInstance(new Configuration());
		countJob.setJarByClass(TextProcessor.class);
		
		/* Make the white list of vocabulary words accessible from the mapper */
		countJob.addCacheFile(new URI(stopWordsSrc));
		countJob.getConfiguration().set(getStopListKey(), new Path(stopWordsSrc).getName());
		
		/* Set up the inputs and mappers */
		countJob.setMapperClass(WordCountMap.class);
		countJob.setMapOutputKeyClass(Text.class);
		countJob.setMapOutputValueClass(WordQuant.class);
		
		FileInputFormat.addInputPath(countJob, new Path(src));
		countJob.setInputFormatClass(TextInputFormat.class);
		
		/* Set up the outputs and reducers */
		countJob.setReducerClass(WordCountReduce.class);
		countJob.setOutputKeyClass(Text.class);
		countJob.setOutputValueClass(Text.class);
		
		
		FileOutputFormat.setOutputPath(countJob, new Path(dest));
		success = countJob.waitForCompletion(true);
		
		// Retrieve the counter value
		quantSymbols = countJob.getCounters().findCounter(TextProcessCounters.QUANT_STOCK_SYMBOLS).getValue();
		
		return (success ? 0 : 1);
	}
	
	private int filterWords(String src, String dest) throws Exception {
		Job filterJob = Job.getInstance(new Configuration());
		
		filterJob.setJarByClass(TextProcessor.class);
		
		/* Set up mapper and input */
		filterJob.setMapperClass(FilterWordsMap.class);
		filterJob.setMapOutputKeyClass(Text.class);
		filterJob.setMapOutputValueClass(WordQuant.class);
		
		filterJob.setInputFormatClass(TextInputFormat.class);
		FileInputFormat.addInputPath(filterJob, new Path(src));
		
		/* Set up reducer and output */
		filterJob.setReducerClass(FilterWordsReduce.class);
		filterJob.setOutputKeyClass(LongWritable.class);
		filterJob.setOutputValueClass(Text.class);
		
		FileOutputFormat.setOutputPath(filterJob, new Path(dest));
		
		
		
		return (filterJob.waitForCompletion(true) ? 0 : 1);
		
	}
	
	// XXX change to make use indices rather than words?
	public int constructVectors(String countSrc, String acceptableSrc, String dest) throws Exception {
		Job constructJob = Job.getInstance(new Configuration());
		
		constructJob.setJarByClass(TextProcessor.class);
		
		/* Make the white list of vocabulary words accessible from the mapper */
		constructJob.addCacheFile(new URI(acceptableSrc));
		constructJob.getConfiguration().set(getWhiteListKey(), new Path(acceptableSrc).getName());
		
		/* Set up mapper and input */
		constructJob.setMapperClass(MatrixConstructionMap.class);
		constructJob.setMapOutputKeyClass(Text.class);
		constructJob.setMapOutputValueClass(WordMeasure.class);
		
		constructJob.setInputFormatClass(TextInputFormat.class);
		FileInputFormat.addInputPath(constructJob, new Path(countSrc));
		
		/* Set up reducer and output */
		constructJob.setReducerClass(VectorConstructionReduce.class);
		constructJob.setOutputKeyClass(Text.class);
		constructJob.setOutputKeyClass(Text.class);
		
		FileOutputFormat.setOutputPath(constructJob, new Path(dest));
		
		/* Stow the number of stock symbols in the context */
		assert (quantSymbols <= Integer.MAX_VALUE);
		assert(quantSymbols > 0);
		constructJob.getConfiguration().setInt(getQuantSymbolsKey(), (int) quantSymbols);
		
		
		return (constructJob.waitForCompletion(true) ? 0 : 1);
	}
	
	public static String getWhiteListKey() {
		return WHITE_LIST_KEY;
	}
	
	public static String getStopListKey() {
		return STOP_LIST_KEY;
	}
	
	
	public static String getQuantSymbolsKey() {
		return QUANT_SYMBOLS_KEY;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			ToolRunner.run(new Configuration(), new TextProcessor(), args);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
