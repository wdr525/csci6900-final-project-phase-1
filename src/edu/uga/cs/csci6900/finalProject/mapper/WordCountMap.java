package edu.uga.cs.csci6900.finalProject.mapper;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import edu.uga.cs.csci6900.finalProject.TextProcessor;
import edu.uga.cs.csci6900.finalProject.io.HadoopFileFilter;
import edu.uga.cs.csci6900.finalProject.io.WordQuant;

public class WordCountMap extends Mapper<LongWritable, Text, Text, WordQuant> {
	
	private static final String STOP_WORD_EXPR = "[a-z0-9']+";
	private static final String SYMBOL_SPLIT_EXPR = "\t";
	private static final String WORD_SPLIT_EXPR ="[^a-z']";

	private HashSet<String> stopWords;
	
	@Override
	protected void map(LongWritable key, Text value,
			org.apache.hadoop.mapreduce.Mapper.Context context)
			throws IOException, InterruptedException {
			HashMap<String, Integer> wordMap = new HashMap<String, Integer>();
			
			String[] primarySplit = value.toString().split(SYMBOL_SPLIT_EXPR);
			
			if (primarySplit.length <= 1) {
				System.err.println("Invalid line detected: \"" + value.toString()+"\"");
				return;
			}
			String[] contentSplit = primarySplit[1].toLowerCase().split(WORD_SPLIT_EXPR);
			
			for (String word : contentSplit) {
				if (stopWords.contains(word)) {
					continue;
				}
					
				
				if (!wordMap.containsKey(word)) {
					wordMap.put(word, 1);
				} else {
					Integer crrntCount = wordMap.get(word);
					wordMap.put(word, new Integer(crrntCount.intValue() + 1));
				}
			}
			
			Text symbol = new Text(primarySplit[0]);
			for (String uniqueWord : wordMap.keySet()) {
				context.write(symbol, new WordQuant(uniqueWord, wordMap.get(uniqueWord).intValue()));
			}
	}

	@Override
	protected void setup(Context context)
			throws IOException, InterruptedException {
		stopWords = new HashSet<String>();
		
		if (loadStopWords(context.getConfiguration().get(TextProcessor.getStopListKey()))) {
			System.out.println("Loaded stop words successfully");
		} else {
			System.err.println("Error: unable to load stop words");
		}
			
	}
	
	private boolean loadStopWords(String locName) {
		File location = new File(locName);
		
		try {
			BufferedReader wordReader = new BufferedReader(new FileReader(location));
			String wordLine;
			
			
			while ((wordLine = wordReader.readLine()) != null) {
				wordLine = wordLine.trim();
				if (wordLine.matches(STOP_WORD_EXPR)) {
					stopWords.add(wordLine);
		
					System.out.println("Loaded stop word: \"" +wordLine+"\"" );
				} else {
					System.err.println("Invalid stop word encountered:\""+wordLine+"\" in file \""+location.getName()+"\"");
				}
			}
			
			wordReader.close();
		} catch (IOException ioe) {
			System.err.println(ioe.getMessage());
		}
		
		return (stopWords.size() > 0);
	}

	
}
