package edu.uga.cs.csci6900.finalProject.mapper;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import edu.uga.cs.csci6900.finalProject.TextProcessor;
import edu.uga.cs.csci6900.finalProject.io.HadoopFileFilter;
import edu.uga.cs.csci6900.finalProject.io.PositionValue;
import edu.uga.cs.csci6900.finalProject.io.WordMeasure;
import edu.uga.cs.csci6900.finalProject.io.WordQuant;

// XXX rename
public class MatrixConstructionMap extends
		Mapper<LongWritable, Text, Text, WordMeasure> {

	
	private static final String WHITE_LIST_ENTRY_EXPR = "\\d+\\s[a-zA-Z0-9']+";
	private static final String SYMBOL_SPLIT_EXPR = "\t";
	private static final String COUNT_SPLIT_EXPR = "\\s";
	
	private int quantSymbols;
	private HashMap<String, QuantIndexPair> whiteList;
	
	@Override
	protected void setup(org.apache.hadoop.mapreduce.Mapper.Context context)
			throws IOException, InterruptedException {
		
		/* Load the number of document symbols from the context */
		quantSymbols = context.getConfiguration().getInt(TextProcessor.getQuantSymbolsKey(), 0);
		assert(quantSymbols > 0);
		
		/* Load the list of acceptable words from the distributed cache */
		loadAcceptableWords(context.getConfiguration().get(TextProcessor.getWhiteListKey()));
		super.setup(context);
	}
	
	@Override
	protected void map(LongWritable key, Text value,
			org.apache.hadoop.mapreduce.Mapper.Context context)
			throws IOException, InterruptedException {
		
		String[] primarySplit = value.toString().split(SYMBOL_SPLIT_EXPR);
		String[] countSplit = primarySplit[1].split(COUNT_SPLIT_EXPR);
		
		WordQuant crrntQuant;
		double measure;
		for (String wqStr : countSplit) {
			crrntQuant = WordQuant.parseWordQuant(wqStr);
			if ((crrntQuant != null) && (whiteList.containsKey(crrntQuant.getWord()))) {
				
				measure = 1 + Math.log10((double) crrntQuant.getCount());
				measure *= Math.log10(1.0+ (quantSymbols/((double)whiteList.get(crrntQuant.getWord()).getQuant())));
				System.out.println("Number of occurrences in stock: "+ crrntQuant.getCount());
				System.out.println("Number of stocks it appears in: " + whiteList.get(crrntQuant.getWord()).getQuant());
				System.out.println("Total number of symbols: "+ quantSymbols);
				//context.write(new Text(primarySplit[0]), new PositionValue(whiteList.get(crrntQuant.getWord()), crrntQuant.getCount()));
				context.write(new Text(primarySplit[0]), new WordMeasure(crrntQuant.getWord(), measure));
			}
		}
		
	}

	private void loadAcceptableWords(String locName) throws IOException {
		int wordIndex = 0;
		File acceptableLocation = new File(locName);
		File[] wordFiles = acceptableLocation.listFiles(new HadoopFileFilter());
		System.out.println("Searching in location: \""+locName+"\"");
		for (String name : acceptableLocation.list()){
			System.out.println("File name: \""+name+"\"");
		}
		
		whiteList = new HashMap<String, QuantIndexPair>();
		
		// Read in words from each file
		for (File f : wordFiles) {
			
			BufferedReader wordReader = new BufferedReader(new FileReader(f));
			String wordLine;
			
			
			while ((wordLine = wordReader.readLine()) != null) {
				if (wordLine.matches(WHITE_LIST_ENTRY_EXPR)) {
					String[] lineContent = wordLine.split(SYMBOL_SPLIT_EXPR);
					whiteList.put(lineContent[1], new QuantIndexPair(Integer.parseInt(lineContent[0]),wordIndex));
					++wordIndex;
					System.out.println("Acceptable word:" +lineContent[1] );
				} else {
					System.err.println("Invalid line encountered:\""+wordLine+"\" in file \""+f.getName()+"\"");
				}
			}
			
			wordReader.close();
				
		}
				
		
	}
	
	private class QuantIndexPair {
		int quant = 0;
		int index = 0;
		
		public QuantIndexPair(int pQuant, int pIndex) {
			quant= pQuant;
			index = pIndex;
		}

		public int getQuant() {
			return quant;
		}

		public void setQuant(int quant) {
			this.quant = quant;
		}

		public int getIndex() {
			return index;
		}

		public void setIndex(int index) {
			this.index = index;
		}
		
		
	}
	
}
